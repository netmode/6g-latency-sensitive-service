import numpy as np
from ultralytics import YOLO
import zmq
from PIL import Image
import io
import json
import os
import torch

import logging
from opentelemetry._logs import set_logger_provider
from opentelemetry.exporter.otlp.proto.grpc._log_exporter import OTLPLogExporter
from opentelemetry.sdk._logs import LoggerProvider, LoggingHandler
from opentelemetry.sdk._logs.export import BatchLogRecordProcessor
from opentelemetry.sdk.resources import Resource

from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleSpanProcessor
from opentelemetry.sdk.resources import Resource
from opentelemetry.trace.status import Status, StatusCode
from opentelemetry.propagate import extract

from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader
from opentelemetry.exporter.otlp.proto.grpc.metric_exporter import OTLPMetricExporter
from opentelemetry import metrics

def initialize_otel_tracer():
    trace.set_tracer_provider(TracerProvider(resource=Resource(attributes={"service.name": "object-detector"})))
    tracer = trace.get_tracer("application")

    exporter = OTLPSpanExporter(endpoint=f"http://{os.getenv('otel_collector_ip')}:{os.getenv('otel_collector_port')}")
    span_processor = SimpleSpanProcessor(exporter)

    trace.get_tracer_provider().add_span_processor(span_processor)

    return tracer

def initialize_otel_logger():
    logging.basicConfig(level=logging.DEBUG)

    logger_provider = LoggerProvider(resource=Resource.create({"service.name": "object-detector"}))
    set_logger_provider(logger_provider)
    exporter = OTLPLogExporter(endpoint=f"http://{os.getenv('otel_collector_ip')}:{os.getenv('otel_collector_port')}")
    logger_provider.add_log_record_processor(BatchLogRecordProcessor(exporter))

    handler = LoggingHandler(level=logging.DEBUG, logger_provider=logger_provider)

    otel_logger = logging.getLogger("otel_logger")
    otel_logger.addHandler(handler)

    return otel_logger

def initialize_otel_meter():
    exporter = OTLPMetricExporter(endpoint=f"http://{os.getenv('otel_collector_ip')}:{os.getenv('otel_collector_port')}", insecure=True)
    metric_reader = PeriodicExportingMetricReader(exporter, export_interval_millis=5000)
    
    meter_provider = MeterProvider(metric_readers=[metric_reader])
    metrics.set_meter_provider(meter_provider)
    
    meter = metrics.get_meter("object-detector")

    return meter


def communicate_with_robotic_arm(socket, command, logger_dict_info):
    try:
        # Send 'Stop' or 'Start' command to the robotic arm
        socket.send_string(command, zmq.DONTWAIT)
        logger.info(f"Command '{command}' was sent to the robotic arm.", extra=logger_dict_info)
        
        # Wait for a reply from the robotic arm for up to 5 seconds
        if socket.poll(timeout=1000):
            message = socket.recv_string()
            logger.info(f"Received reply from the robotic arm: {message}", extra=logger_dict_info)
        else:
            logger.warning("No reply was received from the robotic arm within the expected time frame. The message may not have been sent.", extra=logger_dict_info)
    except zmq.ZMQError as e:
        logger.error(f"An error occurred while communicating with the robotic arm: {e}", extra=logger_dict_info)

def detect(port, model_name, num_threads, robotic_arm_ip, robotic_arm_port):
    # load a pretrained YOLOv8 model: yolov8n.pt, yolov8s.pt, yolov8m.pt, yolov8l.pt, or yolov8x.pt
    # yolov8n.pt is the smallest and fastest model, while yolov8x.pt is the largest and most accurate
    torch.set_num_threads(num_threads)
    model = YOLO(model_name)

    # initialize ZMQ context and create a REP socket
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    # bind to the appropriate IP and port
    socket.bind(f"tcp://*:{port}")
    # set the receive timeout to 2 seconds
    socket.RCVTIMEO = 2000

    # create a REQ socket for communicating with the robotic arm
    robotic_arm_socket = context.socket(zmq.REQ)
    # connect to the robotic arm using the IP address and port number
    robotic_arm_socket.connect(f"tcp://{robotic_arm_ip}:{robotic_arm_port}")
    # flag to keep track of the robotic arm's state
    is_arm_halted=False

    try:
        while True:
            try:
                print("Waiting for a frame...")
                # receive the context and JPEG frame from the frame-sampler
                parts = socket.recv_multipart()
                context_json = parts[0].decode('utf-8')
                trace_context = json.loads(context_json)
                jpeg_data = parts[1]

                print("Received a frame")
            except zmq.error.Again:
                print("Timeout occurred while waiting for a frame.")
                continue
            
            # increment the frame arrival counter
            frame_arrival_counter.add(1, attributes={"pod": pod_name, "workflow": workflow})

            context = extract(trace_context)
            # create a new span for the object detection process using the context extracted from the frame-sampler
            with tracer.start_as_current_span("object-detector-span", context=context, end_on_exit=True) as current_span:
                # get the trace ID from the span context and convert it to a hexadecimal string
                trace_id = current_span.get_span_context().trace_id
                trace_id_hex = hex(trace_id)[2:]

                # create logger dictionary information
                logger_dict_info = {"pod": pod_name, "trace_id": trace_id_hex, "workflow": workflow}
                
                # log the received trace ID
                logger.info(f"Received frame from frame-sampler", extra=logger_dict_info)

                # send reply back to client
                socket.send_string("OK")
                print("Sent Acknowledgement")

                # decode JPEG data using PIL
                img = Image.open(io.BytesIO(jpeg_data))

                # resize the image so its dimensions are multiples of 32
                width, height = img.size
                new_width = (width // 32) * 32
                new_height = (height // 32) * 32
                img = img.resize((new_width, new_height))

                # convert the PIL Image to a numpy array
                img = np.array(img)
                
                result = model.predict(img, imgsz=(new_height, new_width), verbose=False)[0] # set specific input image size

                # https://www.freecodecamp.org/news/how-to-detect-objects-in-images-using-yolov8/
                detected_objects = []
                for box in result.boxes:
                    # the class of the detected object
                    obj_class = result.names[box.cls[0].item()]
                    detected_objects.append(obj_class)
                
                # log the detected objects and the speed of the object detection process
                prediction_info = (
                    f"Detected objects: {detected_objects if detected_objects else '(no detections)'}\n"
                    f"Speed: {result.speed['preprocess']:.1f}ms preprocess, "
                    f"{result.speed['inference']:.1f}ms inference, "
                    f"{result.speed['postprocess']:.1f}ms postprocess per image"
                )
                logger.info(prediction_info, extra=logger_dict_info)

                # check if a person was detected in the frame
                if 'person' in detected_objects:
                    logger.warning('ALERT!!! A person was detected in the frame.', extra=logger_dict_info)
                    
                    # send a 'Stop' command to the robotic arm if a person was detected in the frame 
                    # and the robotic arm is not already halted
                    if not is_arm_halted:
                        # communicate with the robotic arm to stop its operation
                        communicate_with_robotic_arm(robotic_arm_socket, "Stop", logger_dict_info)
                        # update the state of the robotic arm
                        is_arm_halted = True
                else:
                    # send a 'Start' command to the robotic arm if no person was detected in the frame 
                    # and the robotic arm is halted
                    if is_arm_halted:
                        # communicate with the robotic arm to start its operation
                        communicate_with_robotic_arm(robotic_arm_socket, "Start", logger_dict_info)
                        # update the state of the robotic arm
                        is_arm_halted = False

                # set the status of the span to OK and add attributes
                current_span.set_status(Status(StatusCode.OK))
                current_span.set_attribute("workflow", workflow)
                current_span.set_attribute("pod", pod_name)

                # increment the frame sent counter
                frame_sent_counter.add(1, attributes={"pod": pod_name, "workflow": workflow})

    except KeyboardInterrupt:
        print("Interrupt received, stopping...")
    finally:
        # clean up the socket and the ZMQ context
        socket.close()
        context.term()

if __name__ == '__main__':
    # get the pod name and workflow
    pod_name = os.getenv('HOSTNAME')
    workflow = os.getenv('workflow')

    print(f"Pod {pod_name} has just started...")

    # Initialize the OpenTelemetry components
    tracer = initialize_otel_tracer()
    logger = initialize_otel_logger()
    meter = initialize_otel_meter()

    # create counters for frame arrival and frame sent
    frame_arrival_counter = meter.create_counter(
        name="frame_arrival_count",
        description="Counts the number of frames that have arrived",
        unit="1"
    )
    frame_sent_counter = meter.create_counter(
        name="frame_sent_count",
        description="Counts the number of frames that have been sent",
        unit="1"
    )

    # get the environment variables
    args = {'port': os.getenv('port'), 
        'yolo_model': os.getenv('yolo_model'),
        'num_threads': int(os.getenv('num_threads')),
        'robotic_arm_ip': os.getenv('robotic_arm_ip'),
        'robotic_arm_port': os.getenv('robotic_arm_port')
    }

    print(f'Receiving sampled frames at UDP port {args["port"]}')
    print(f'Using pre-trained model: {args["yolo_model"]}')
    print(f'Number of threads: {args["num_threads"]}')
    print(f'Robotic arm IP: {args["robotic_arm_ip"]}')
    print(f'Robotic arm port: {args["robotic_arm_port"]}')
    
    detect(args["port"], args["yolo_model"], args["num_threads"], args["robotic_arm_ip"], args["robotic_arm_port"])