import traceback
import multiprocessing
import re
import zmq
import struct
import time
import os
import json

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

import logging
from opentelemetry._logs import set_logger_provider
from opentelemetry.exporter.otlp.proto.grpc._log_exporter import OTLPLogExporter
from opentelemetry.sdk._logs import LoggerProvider, LoggingHandler
from opentelemetry.sdk._logs.export import BatchLogRecordProcessor
from opentelemetry.sdk.resources import Resource

from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleSpanProcessor
from opentelemetry.sdk.resources import Resource
from opentelemetry.trace.status import Status, StatusCode
from opentelemetry.propagate import inject

from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader
from opentelemetry.exporter.otlp.proto.grpc.metric_exporter import OTLPMetricExporter
from opentelemetry import metrics

def initialize_otel_tracer():
    trace.set_tracer_provider(TracerProvider(resource=Resource(attributes={"service.name": "frame-sampler"})))
    tracer = trace.get_tracer("application")

    exporter = OTLPSpanExporter(endpoint=f"http://{os.getenv('otel_collector_ip')}:{os.getenv('otel_collector_port')}")
    span_processor = SimpleSpanProcessor(exporter)

    trace.get_tracer_provider().add_span_processor(span_processor)

    return tracer

def initialize_otel_logger():
    logging.basicConfig(level=logging.DEBUG)

    logger_provider = LoggerProvider(resource=Resource.create({"service.name": "frame-sampler"}))
    set_logger_provider(logger_provider)
    exporter = OTLPLogExporter(endpoint=f"http://{os.getenv('otel_collector_ip')}:{os.getenv('otel_collector_port')}")
    logger_provider.add_log_record_processor(BatchLogRecordProcessor(exporter))

    handler = LoggingHandler(level=logging.DEBUG, logger_provider=logger_provider)

    otel_logger = logging.getLogger("otel_logger")
    otel_logger.addHandler(handler)

    return otel_logger

def initialize_otel_meter():
    exporter = OTLPMetricExporter(endpoint=f"http://{os.getenv('otel_collector_ip')}:{os.getenv('otel_collector_port')}", insecure=True)
    metric_reader = PeriodicExportingMetricReader(exporter, export_interval_millis=5000)
    
    meter_provider = MeterProvider(metric_readers=[metric_reader])
    metrics.set_meter_provider(meter_provider)
    
    meter = metrics.get_meter("frame-sampler")

    return meter

def on_message(bus: Gst.Bus, message: Gst.Message, loop: GLib.MainLoop):
    msg_type = message.type

    # handle different types of messages
    if msg_type == Gst.MessageType.EOS:
        print("End of stream")
        loop.quit()
    elif msg_type == Gst.MessageType.ERROR:
        error, debug_info = message.parse_error()
        print(f"Error: {error.message}, Debug Info: {debug_info}")
        loop.quit()
    elif msg_type == Gst.MessageType.WARNING:
        warning, debug_info = message.parse_warning()
        print(f"Warning: {warning.message}, Debug Info: {debug_info}")

    return True

def on_handoff(element, buffer, spans):
    # map the buffer for reading
    result, mapinfo = buffer.map(Gst.MapFlags.READ)
    if not result:
        return Gst.PadProbeReturn.OK # return if mapping fails

    # extract the marker bit from the RTP header
    marker_bit = (mapinfo.data[1] & 0x80) >> 7
    if marker_bit: # process only the last fragment (marker bit set)
        # set the current time in nanoseconds as the frame ID
        frame_id = time.time_ns()

        # set the frame ID as a reference timestamp meta on the buffer
        reference_caps = Gst.Caps.from_string("image/jpeg")
        buffer.add_reference_timestamp_meta(reference_caps, frame_id, Gst.CLOCK_TIME_NONE)

        # start a trace span for the frame
        with tracer.start_as_current_span("frame-sampler-span", end_on_exit=False) as current_span:
            # retrieve trace ID in hexadecimal format
            trace_id = current_span.get_span_context().trace_id
            trace_id_hex = hex(trace_id)[2:]
            
            # record the span in the dictionary of spans with the frame ID as the key
            spans[frame_id] = current_span

            # log the frame arrival event
            logger_dict_info = {"pod": pod_name, "trace_id": trace_id_hex}
            logger.info(f"Frame received from source", extra=logger_dict_info)
        
        # increment the frame arrival counter
        frame_arrival_counter.add(1, attributes={"pod": pod_name, "workflow": workflow})

    # unmap the buffer
    buffer.unmap(mapinfo)


def on_sample(appsink, args, spans):
    # get the sample from the appsink
    sample = appsink.emit("pull-sample")
    buffer = sample.get_buffer()

    # map the buffer for reading
    success, info = buffer.map(Gst.MapFlags.READ)
    if not success:
        return Gst.FlowReturn.ERROR

    # get the JPEG data from the buffer
    jpeg_data = info.data

    # unmap the buffer
    buffer.unmap(info)

    # retrieve the reference timestamp meta from the buffer serving as the frame ID
    reference_caps = Gst.Caps.from_string("image/jpeg")  
    meta = buffer.get_reference_timestamp_meta(reference_caps)
    if not meta:
        return Gst.FlowReturn.ERROR
    
    try:
        # access the frame ID
        frame_id = meta.timestamp
        # retrieve the span associated with the frame ID and remove it from the dictionary of spans
        span = spans[frame_id]
        del spans[frame_id]
    except KeyError:
        # return with no error if the frame ID is not found
        return Gst.FlowReturn.OK

    # retrieve the trace ID
    trace_id_hex = hex(span.get_span_context().trace_id)[2:]

    # create logger dictionary information
    logger_dict_info = {"pod": pod_name, "trace_id": trace_id_hex, "workflow": workflow}

    # initialize ZMQ context and create a REQ socket
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    
    # connect to object-detector destination
    socket.connect(f"tcp://{args['dest']}:{args['dport']}")
    print(f"Connected to object-detector")

    context_data = {}
    # continue with the span
    with trace.use_span(span, end_on_exit=True):
        # inject the context data into the span for propagation to the object-detector
        inject(context_data)

        # convert the context data to JSON
        context_json = json.dumps(context_data)

        sent_timestamp = time.time_ns()

        # send the context data and JPEG frame to the object-detector
        socket.send_multipart([context_json.encode('utf-8'), jpeg_data])
        print(f"Send frame to object-detector")

        # get the reply from the object-detector
        socket.recv()
        print(f"Received reply from object-detector")

        # clean up here
        socket.close()
        context.term()
        print(f"Closed connection to object-detector")

        # log the frame sent event
        logger.info("Sent frame to object-detector", extra=logger_dict_info)
        
        # set the status of the span to OK and add attributes
        span.set_status(Status(StatusCode.OK))
        span.set_attribute("pod", pod_name)
        span.set_attribute("workflow", workflow)
        span.set_attribute("sent_timestamp", sent_timestamp)

        # increment the frame sent counter
        frame_sent_counter.add(1, attributes={"pod": pod_name, "workflow": workflow})

    return Gst.FlowReturn.OK


def run_pipeline(pipeline_desc: str, args: dict):
    print(f'Receiving MJPG video stream at UDP port {args["port"]}\n'\
             f'Sampling rate: {args["framerate"]} frames/sec\n'\
             f'Sending samples at {args["dest"]}:{args["dport"]}')

    spans = dict() # {frame_id: span}
    # create a new pipeline based on command line syntax
    pipeline = Gst.parse_launch(pipeline_desc)

    # retrieve the bus associated with the pipeline
    bus = pipeline.get_bus()
    # allow bus to emit signals for events
    bus.add_signal_watch()

    identity1 = pipeline.get_by_name("id")
    # connect the callback to the "handoff" signal of identity1
    identity1.connect("handoff", on_handoff, spans)

    # connect the on_sample callback to the pull-sample signal
    sink = pipeline.get_by_name("sink")
    sink.set_property("emit-signals", True)
    sink.connect("new-sample", on_sample, args, spans)

    # start pipeline
    pipeline.set_state(Gst.State.PLAYING)
    print("Pipeline started")

    # create main event loop
    loop = GLib.MainLoop()
    # add callback to specific signal
    bus.connect("message", on_message, loop)

    try:
        loop.run()
    except KeyboardInterrupt:
        print('Terminating...')
    except Exception as e:
        print(f"Exception: {e}")
        # print exception information and stack trace entries
        traceback.print_exc()
    finally:
        # stop pipeline
        pipeline.set_state(Gst.State.NULL)
        loop.quit()
        print("Pipeline stopped")

def update_rate(pipeline_desc:str, 
                    gst_process: multiprocessing.Process, 
                    args: dict):
    # control port
    c_port = args["control_port"]

    context = zmq.Context()
    # create REP socket
    rate_socket =  context.socket(zmq.REP)
    # bind socket to port
    rate_socket.bind(f'tcp://*:{c_port}')
    
    # create a poller and register the socket for polling
    poller = zmq.Poller()
    poller.register(rate_socket, zmq.POLLIN)
   
    print(f"Listening for interval update requests on port {c_port}")
   
    while True:
        try:
            # poll for events
            events = dict(poller.poll())
            # check for events on rate_socket
            if rate_socket in events and events[rate_socket] == zmq.POLLIN:
                new_rate = rate_socket.recv_string()
                print(f"Received new sampling rate value: {new_rate} frames/s")

                # kill the child process
                gst_process.terminate()
                # wait for child process to finish
                gst_process.join()
                # use regular expression to replace value for frame-rate
                pipeline_desc = re.sub(
                    r'framerate=\d+/\d+',
                    f'framerate={new_rate}', 
                    pipeline_desc
                )
                
                # update args
                args["framerate"] = new_rate
                # start a separate process running the gstreamer pipeline
                gst_process = multiprocessing.Process(
                    target=run_pipeline, args=(pipeline_desc, args))
                gst_process.start()

                # send a response back to the client if needed
                rate_socket.send_string(f"Changed sampling rate to {new_rate} frames/s")
                print(f"Changed sampling rate to {new_rate} frames/s")
        except KeyboardInterrupt:
            # kill the child process
            gst_process.terminate()
            # wait for child process to finish
            gst_process.join()
            break

if __name__ == '__main__':
    # get the pod name and workflow
    pod_name = os.getenv('HOSTNAME')
    workflow = os.getenv('workflow')

    print(f"Pod {pod_name} has just started...")

    # initialize OpenTelemetry components
    tracer = initialize_otel_tracer()
    logger = initialize_otel_logger()
    meter = initialize_otel_meter()

    # create counters for frame arrival and frame sent
    frame_arrival_counter = meter.create_counter(
        name="frame_arrival_count",
        description="Counts the number of frames that have arrived",
        unit="1"
    )
    frame_sent_counter = meter.create_counter(
        name="frame_sent_count",
        description="Counts the number of frames that have been sent",
        unit="1"
    )
    
    # get the environment variables
    args = {'port': os.getenv('port'), 
        'framerate': os.getenv('framerate'),
        'dest': os.getenv('destination_ip'),
        'dport': os.getenv('destination_port'),
        'control_port': os.getenv('framerate_port'),
    }

    # initialize GStreamer library
    Gst.init(None)

    # define the GStreamer pipeline
    pipeline_desc = (
        f'udpsrc port={args["port"]} ! '
        'application/x-rtp, encoding-name=JPEG, payload=26 ! '
        'identity name=id ! '
        'queue ! '
        'rtpjpegdepay ! '
        'queue ! '
        'jpegparse ! '
        'videorate ! '
        f'image/jpeg, framerate={args["framerate"]} ! '
        'queue ! '
        'appsink name=sink sync=False'
    )

    # start a separate process running the gstreamer pipeline
    gst_process = multiprocessing.Process(
        target=run_pipeline, args=(pipeline_desc, args))
    gst_process.start()

    update_rate(pipeline_desc, gst_process, args)