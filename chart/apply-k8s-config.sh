#!/bin/bash

################################################################################
# Script Description:
# This script is designed to automate the configuration of an existing 
# Kubernetes cluster. It uses Helm, a package manager for Kubernetes, to 
# deploy services, config-maps, and deployments.
#
# The script kicks off by deploying various services, with special emphasis on 
# the "mediamtx-service," a critical component for cluster operation. After 
# obtaining its external IP, the script seamlessly integrates it into the 
# Kubernetes cluster configuration within the "frontend-config-map" and 
# "mediamtx" pod.

# If the user confirms the presence of a ROS (Robot Operating System) cluster 
# with a ROS bridge server, the script prompts for the ROS Bridge Server IP. This 
# IP is validated and incorporated into the "frontend-config-map". However, it's 
# important to note that configuring the ROS cluster is optional and not 
# mandatory for overall Kubernetes cluster setup.

# If the user confirms the presence of a robotic arm, the script prompts for the robotic 
# arm IP. This IP is validated and incorporated into the "object-detector-config-map". 
# However, it's important to note that configuring the robotic arm is optional and not 
# mandatory for overall Kubernetes cluster setup.

# Usage:
# ./apply-k8s-config.sh --chart <chart-name> --namespace <namespace-name>
################################################################################

#  set $namespace,$chart from flags --chart, --namespace
namespace=""
chart=""

while [ "$1" != "" ]; do
    case $1 in
        --chart )          shift
                                chart=$1
                                ;;
        --namespace )           shift
                                namespace=$1
                                ;;
        * )                     echo -e "\033[0;31mError: Unknown flag $1\033[0m ❌"
    esac
    shift
done

if [ -z "$chart" ] || [ -z "$namespace" ]; then
    echo -e "\033[0;31mError: Both --chart and --namespace flags are required\033[0m ❌"
    exit 1
fi

echo "👉 Chart Name set to $chart"
echo "👉 Namespace set to $namespace"

# check if kubernetes is installed and in path. If not, print an error message and exit the script.
if command -v kubectl > /dev/null 2>&1; then
    echo "👉 kubectl version:$(kubectl version --client | grep Client | awk -F : '{print $2}'). Kubernetes is installed ✅"
else
    echo -e "\033[0;31mError: Kubernetes is not installed or not in the PATH. Please install Kubernetes before running this script\033[0m ❌"
    exit 1
fi

# check if kubernetes is running. If not, print an error message and exit the script.
if kubectl cluster-info > /dev/null 2>&1; then
    echo "👉 KUBECONFIG file path: $KUBECONFIG"
    echo "👉 $(kubectl cluster-info | grep 'Kubernetes control plane' | sed 's,\x1B\[[0-9;]*[a-zA-Z],,g'). Kubernetes is running ✅"
else
    echo -e "\033[0;31mError: Kubernetes is not running or not accessible. Please start Kubernetes before running this script\033[0m ❌"
    exit 1
fi

# check if Helm is installed. If not, print an error message and exit the script.
if helm version > /dev/null 2>&1; then
    echo "Helm is installed ✅"
else
    echo -e "\033[0;31mError: Helm is not installed. Please install Helm before running this script\033[0m ❌"
    exit 1
fi

# check if the namespace exists. if yes proceed, else create the namespace
if kubectl get namespace "$namespace" > /dev/null 2>&1; then
    echo "Namespace $namespace already exists ✅"
else
    echo "Namespace $namespace does not exist. Creating namespace $namespace.... 🚀"
    kubectl create namespace "$namespace" > /dev/null 2>&1
    echo "Namespace $namespace created successfully ✅"
fi
# check if the chart exists. if yes proceed, else exit the script
if [ -d "$chart" ]; then
    echo "Chart $chart exists ✅"
else
    echo -e "\033[0;31mError: Chart $chart does not exist. Please ensure the chart exists in the current directory\033[0m ❌"
    exit 1
fi

# set the namespace variable in the $chart/values.yaml file to $namespace
sed -i "s/namespace: .*/namespace: $namespace/g" $chart/values.yaml

helm uninstall "app-services" -n $namespace> /dev/null 2>&1 || true
helm uninstall "app-core" -n $namespace> /dev/null 2>&1 || true
helm uninstall "app-propagation" -n $namespace> /dev/null 2>&1 || true

# this function validates the format of the ROS bridge server IP.
validate_ip() {
    local ip=$1
    local ip_regex="^([0-9]{1,3}\.){3}[0-9]{1,3}$"

    # check if the IP matches the regular expression.
    if [[ $ip =~ $ip_regex ]]; then
        # Split the IP into octets and check each one.
        IFS='.' read -ra octets <<< "$ip"
        for octet in "${octets[@]}"; do
            # If an octet is out of range (0-255), it's an invalid IP.
            if ((octet < 0 || octet > 255)); then
                echo -e "\033[0;31mError: Invalid IP address: $ip\033[0m"
                exit 1  # Exit the script if IP is invalid
            fi
        done
    else
        echo -e "\033[0;31mError: Invalid IP address: $ip\033[0m"
        exit 1  # exit the script if IP is invalid
    fi
}

# ask the user about the ROS cluster
read -p "Are you currently operating a ROS cluster with the ROS bridge server enabled? (yes/no): " has_ros_cluster

# if the user has a ROS cluster, ask for the ROS Bridge Server IP and validate it. 
# if answer is neither yes nor no, exit the script.
if [ "$has_ros_cluster" != "yes" ] && [ "$has_ros_cluster" != "no" ]; then
    echo -e "\033[0;31mError: Invalid input. Please enter either 'yes' or 'no'\033[0m ❌"
    exit 1
fi
if [ "$has_ros_cluster" == "yes" ]; then
    read -p "Enter the ROS Bridge Server IP: " ros_bridge_server_ip
    validate_ip "$ros_bridge_server_ip"
    echo "Ros Bridge Server IP: $ros_bridge_server_ip ✅"
    echo ""
fi
# if no ROS cluster available, print a message that ros bridge server connection is disabled by user
if [ "$has_ros_cluster" == "no" ]; then
    echo -e "\033[0;33mROS Bridge Server connection is disabled by the user\033[0m ⚠️"
fi

# ask the user about the robotic arm
read -p "Are you currently operating a robotic arm? (yes/no): " has_robotic_arm

# if the user has a robotic arm, ask for the robotic arm IP and validate it. 
# if answer is neither yes nor no, exit the script.
if [ "$has_robotic_arm" != "yes" ] && [ "$has_robotic_arm" != "no" ]; then
    echo -e "\033[0;31mError: Invalid input. Please enter either 'yes' or 'no'\033[0m ❌"
    exit 1
fi
if [ "$has_robotic_arm" == "yes" ]; then
    read -p "Enter the robotic arm IP: " robotic_arm_ip
    validate_ip "$robotic_arm_ip"
    echo "Robotic arm IP: $robotic_arm_ip ✅"
    echo ""
fi
# if no robotic arm exists, print a message that robotic arm connection is disabled by user
if [ "$has_robotic_arm" == "no" ]; then
    echo -e "\033[0;33mRobotic arm connection is disabled by the user\033[0m ⚠️"
fi

workflows=""
read -p "Choose workflows to be enabled (1/2/both): " workflows
case "$workflows" in
  1)
    workflows="workflow-1"
    ;;
  2)
    workflows="workflow-2"
    ;;
  both)
    workflows="workflow-1,workflow-2"
    ;;
  *)
    echo -e "\033[0;31mInvalid workflow. Valid workflows are: 1/2/both\033[0m ❌"
    exit 1
    ;;
esac

# try to install the services using Helm
# if the installation fails, print an error message and exit the script.
if helm install app-services $chart \
  -f $chart/values/service-values.yaml \
  --set enabledWorkflows="{$workflows}" \
  -n $namespace; then
    echo -e "\033[0;32mServices were applied successfully\033[0m"
else
    echo -e "\033[0;31mError: Failed to apply services. Please review the configuration and attempt the installation again\033[0m ❌"
    exit 1
fi

# # wait until the mediamtx_service IP is created if workflow-2 is enabled
mediamtx_service_ip=""
if [[ "$workflows" == *"workflow-2"* ]]; then
    while [ -z "$mediamtx_service_ip" ]; do
        mediamtx_service_ip=$(kubectl get service mediamtx-service -n $namespace -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
        # if the IP is not yet created, wait for 2 seconds before checking again.
        [ -z "$mediamtx_service_ip" ] && sleep 2
    done
    echo ""
    echo "Streaming Server IP: $mediamtx_service_ip"
    echo ""
fi

# try to apply the Helm installation for config-maps and deployments.
# if the installation fails, print an error message and exit the script.
if helm install app-core $chart \
  -f $chart/values/config-map-values.yaml \
  -f $chart/values/deployment-values.yaml \
  --set configMaps[0].data.ros_bridge_server_ip=$ros_bridge_server_ip \
  --set configMaps[2].data.robotic_arm_ip=$robotic_arm_ip \
  --set configMaps[0].data.streaming_server_ip=$mediamtx_service_ip:8889 \
  --set deployments[1].env[0].value=$mediamtx_service_ip \
  --set enabledWorkflows="{$workflows}" \
  -n $namespace; then
    echo -e "\033[0;32mConfigmaps and deployments were applied successfully\033[0m"
else
    echo -e "\033[0;31mError: Failed to apply either configmaps or deployments. Please review the configuration and attempt the installation again\033[0m ❌"
    exit 1
fi

# print a success message when the configuration is completed.
echo ""
echo -e "\033[0;32mConfiguration completed successfully\033[0m ✅"

# print the URL to access the frontend application if workflow-2 is enabled
if [[ "$workflows" == *"workflow-2"* ]]; then
    echo ""
    echo -e "Please go to \033[0;33mhttp://$mediamtx_service_ip:5000/streaming\033[0m to access the frontend application 🚀"
fi