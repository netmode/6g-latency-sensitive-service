import argparse
import ipaddress
import zmq

# Function to parse command-line arguments
def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Client socket for connecting to robotic arm server')
    
    # IP address of the robotic arm
    parser.add_argument('ip',
        type=ipaddress.IPv4Address, 
        help='IP address of robotic arm server, e.g., 192.168.100.69')
    
    # Port number on which the robotic arm is listening (optional)
    parser.add_argument('-p', '--port',
        type=int,
        default=5580,
        help='TCP port number to which the data will be received, e.g., 5555')

    args = parser.parse_args()

    return args

# Function to communicate with the robotic arm
def communicate_with_robotic_arm(robotic_arm_ip, robotic_arm_port):
    valid_commands = ["Start", "Stop"]
    
    try :
        # Initialize the ZeroMQ context
        context = zmq.Context()
        # Create a REQ socket
        socket = context.socket(zmq.REQ)
        # Connect to the robotic arm using the IP address and port number
        socket.connect(f"tcp://{str(robotic_arm_ip)}:{robotic_arm_port}")

        while True:
            # Ask the user for a command to send to the robotic arm
            command = input(f'Please give a [{"/".join(valid_commands)}] command to the robotic arm: ').strip()
            
            # Check if the command is valid
            if command not in valid_commands:
                print("Invalid command. Please enter a valid command.")
                print("Valid commands are:", ', '.join(valid_commands))
                continue

            # If the command is valid, send it to the robotic arm
            socket.send_string(command)
            
            
            # Wait for a reply from the robotic arm for up to 5 seconds
            if socket.poll(timeout=5000):
                message = socket.recv_string()
                print(f"Received reply: {message}")
            else:
                print("No reply received. The message may not have been sent.")
    except zmq.ZMQError as e:
        # If there is a ZeroMQ error, print the error and retry after 5 seconds
        print("ZMQ Socket error: ", e)
    finally:
        # Close the socket and terminate the context
        socket.close()
        context.term()


if __name__ == '__main__':
    # Parse the command-line arguments
    args = parse_arguments()

    # Communicate with the robotic arm using the parsed arguments
    communicate_with_robotic_arm(args.ip, args.port)